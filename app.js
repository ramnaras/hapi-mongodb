const Hapi = require("hapi");
const Joi = require("joi");
const Mongoose = require("mongoose");

const server = new Hapi.Server({ host: "localhost", port: "3003" });

Mongoose.connect("mongodb://localhost/nodetest");

const PersonModel = Mongoose.model("person", {
  firstName: String,
  lastName: String,
});

server.route({
  method: "POST",
  path: "/person",
  options: {
    validate: {
      payload: {
        firstName: Joi.string().min(3).required(),
        lastName: Joi.string().required(),
      },
      failAction: (request, h, error) => {
        return error.isJoi
          ? h.response(error.details[0]).takeover()
          : h.response(error).takeover();
      },
    },
  },
  handler: async (request, h) => {
    try {
      console.log(request.payload);
      var person = new PersonModel(request.payload);
      var result = await person.save();
      return h.response(result);
    } catch (error) {
      console.log(error);
      return h.response(error).code(500);
    }
  },
});

server.route({
  method: "GET",
  path: "/people",
  handler: async (request, h) => {
    try {
      console.log(request.payload);
      var people = await PersonModel.find().exec();
      return h.response(people);
    } catch (error) {
      console.log(request.payload);
      return h.response(error).code(500);
    }
  },
});

server.route({
  method: "GET",
  path: "/person/{id}",
  handler: async (request, h) => {
    try {
      console.log(request.payload);
      var people = await PersonModel.find({
        _id: request.params.id,
      }).exec();
      return h.response(people);
    } catch (error) {
      console.log(request.payload);
      return h.response(error).code(500);
    }
  },
});

server.route({
  method: "PUT",
  path: "/person/{id}",
  options: {
    validate: {
      payload: {
        firstName: Joi.string().optional(),
        lastName: Joi.string().optional(),
      },
      failAction: (request, h, error) => {
        return error.isJoi
          ? h.response(error.details[0]).takeover()
          : h.response(error).takeover();
      },
    },
  },
  handler: async (request, h) => {
    try {
      var result = await PersonModel.findByIdAndUpdate(
        request.params.id,
        request.payload,
        { new: true }
      );
      return h.response(result);
    } catch (error) {
      console.log(request.payload);
      return h.response(error).code(500);
    }
  },
});

server.route({
  method: "DELETE",
  path: "/person/{id}",
  handler: async (request, h) => {
    try {
      console.log(request.payload);
      var people = await PersonModel.findByIdAndDelete({
        _id: request.params.id,
      }).exec();
      return h.response(people);
    } catch (error) {
      console.log(request.payload);
      return h.response(error).code(500);
    }
  },
});

server.start();
